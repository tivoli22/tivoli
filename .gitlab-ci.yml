image: timimages/ci

#variables:
#  DOCKER_HOST: tcp://docker:2376
#  DOCKER_TLS_CERTDIR: "/certs"

stages:
  - lint
  - test
  - deploy

.setup: &setup
  - docker info
  - docker-compose --version
  - apk add --update bash git
  - cp variables.sh.template variables.sh
  - chmod +x variables.sh
  - sed -i 's/echo variables.sh/#echo variables.sh/' variables.sh
  - sed -i 's/COMPOSE_PROFILES=prod/COMPOSE_PROFILES=test/' variables.sh
  - ./dc pull --quiet tim
  - ./dc run --rm --no-deps --workdir /service tim mypy -p timApp -p tim_common
  - ./dc pull --quiet
  - ./npmi
  - ./js

.dockervars: &dockervars
  DOCKER_DRIVER: overlay2
  GIT_SUBMODULE_STRATEGY: normal

browser_tests:
  variables:
   <<: *dockervars
  services:
  - docker:dind
  stage: test
  before_script:
  - *setup
  script:
  - TEST_COMMAND="python3 tests/ci_browser_test_runner.py" ./dc up --exit-code-from tests --abort-on-container-exit tests
  artifacts:
    name: failed_screenshots
    when: always
    paths:
    - screenshots
    - sql_logs
  timeout: 40m  # usually takes less than 40 minutes
  retry:
    max: 2
    when:
      - stuck_or_timeout_failure
      - runner_system_failure

other_tests:
  variables:
   <<: *dockervars
  services:
  - docker:dind
  stage: test
  before_script:
  - *setup
  script:
  - TEST_COMMAND="python3 -m unittest discover -v tests/unit 'test_*.py' ." ./dc up --exit-code-from tests --abort-on-container-exit tests
  - TEST_COMMAND="python3 -m unittest discover -v tests/db 'test_*.py' ." ./dc up --exit-code-from tests --abort-on-container-exit tests
  - TEST_COMMAND="python3 -m unittest discover -v tests/server 'test_*.py' ." ./dc up --exit-code-from tests --abort-on-container-exit tests

code_navigation:
  stage: test  # not really a test, but we want to execute this in parallel
  image: node:lts
  script:
  - cd timApp
  - npm install --unsafe-perm
  - npm install -g @sourcegraph/lsif-tsc
  - lsif-tsc -p .
  artifacts:
    reports:
      lsif: timApp/dump.lsif
  allow_failure: true  # https://gitlab.com/gitlab-org/gitlab/-/issues/224101

ts_lint:
  stage: lint
  image: node:lts-slim
  script:
  - cd timApp
  - npm install --unsafe-perm
  - npm run lint

stats:
  stage: test
  image: node:lts-slim
  script:
  - cd timApp
  - npm install --unsafe-perm
  - npm install -g webpack-bundle-analyzer
  - npm run statshtml
  artifacts:
    name: webpack_stats
    paths:
    - timApp/report.html

jsrunner_lint:
  stage: lint
  image: node:14  # cannot use lts-slim here; some part of this requires Python; use Node 14 because of old lockfile
  script:
  - cd timApp
  - npm install --unsafe-perm
  - cd modules/jsrunner/server
  - npm install --unsafe-perm
  - npm run lint

ts_format:
  stage: lint
  image: node:14-slim # Use old Node temporarily because of old package versions
  script:
  - cd timApp
  - rm package-lock.json
  - npm install --no-package-lock --no-save prettier@2.4.1
  - npm run checkformat

py_format:
  stage: lint
  image: python:3.10-slim
  script:
  - pip install black==22.3.0
  - black --check .

build_docs:
  stage: deploy
  trigger:
    project: tim-jyu/tim-api-docs
    branch: main
  allow_failure: true
  rules:
    - if: $CI_PROJECT_NAMESPACE == "tim-jyu" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
